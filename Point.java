public class Point extends Sujet {
    protected int x, y;

    public Point(int x, int y) {
        super();
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void setX(int x) {
        this.x = x;
        notifieObservateur("x", x);
    }

    public void setY(int y) {
        this.y = y;
        notifieObservateur("y", y);
    }

}
/**Point: (10, 15)
Tapez :
x pour modifier X
y pour modifier Y
q pour quitter
x
Entrez la coordonnée X : 13
Point: (13, 15)
Observation : PAS DE MODIFICATION
Tapez :
x pour modifier X
y pour modifier Y
q pour quitter
y
Entrez la coordonnée Y : 26
Point: (13, 26)
Observation : PAS DE MODIFICATION
Tapez :
x pour modifier X
y pour modifier Y
q pour quitter
q**//